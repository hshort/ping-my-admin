<?php
/*
 *  Copyright 2017 CERN
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
   <head>
      <meta charset="UTF-8">
      <title>Sirtfi Dashboard</title>
      <!-- Making cursor hand pointer in table row -->
      <style>
         tr {cursor:pointer;}
         .container {
         padding-left:30px;
         padding-right:30px;
         }
         div {color:#003F5D}
      </style>
      <!-- Required meta tags -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <!-- Bootstrap CSS -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
      <!--Boostrap Css Overrides -->
      <link rel="stylesheet" type="text/css" href="css/style.css">
      <!-- Latest compiled JavaScript -->
      <script src="https:///maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
      <!-- JQuery -->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
      <!-- Datatables styling sheets -->
      <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css"/>
      <!-- Datatables js -->
      <script type="text/javascript" src="https://cdn.datatables.net/v/dt/jq-2.2.3/dt-1.10.12/datatables.min.js"></script>
      <script type="text/javascript" src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
      <!-- Store SP as javascript variable if recognised (must manually update this table) -->
      <script>
         var sp = "<?php
            if (in_array(strtoupper(htmlspecialchars($_GET['sp'])), array("CERN"))) {
                echo strtoupper($_GET['sp']);
            } else {
                echo "a Service Provider in eduGAIN";
            }
            ?>";

      </script>
   </head>
   <body >
      <!-- Nav bar -->
      <div class="row">
         <div class="col">
            <nav class="navbar navbar-default" style="border-color:#FFC277;background-color:#FFC277">
               <div class="container-fluid">
                  <div class="navbar-header">
                     <a class="navbar-brand" href="http://sirtfi.cern.ch/index.php"></a>
                  </div>
                  <ul class="nav navbar-nav navbar-right">
                     <li><a href="/sirtfi-metadata.xml" data-toggle="modal">Metadata</a></li>
                  </ul>
               </div>
            </nav>
         </div>
      </div>
      <!-- Heading -->
      <div class="container">
         <div class="row mt-2">
            <div class="col">
               <h1>The Security Incident Response Trust Framework</h1>
               <h2>for Federated Identity</h2>
               <p>
                  <?php if (isset($_GET['sp'])) {
                     echo "You need Sirtfi to access ";
                     echo strtoupper(htmlspecialchars($_GET['sp'],ENT_QUOTES, 'UTF-8'));
                     echo "!";
                     }else{
                     echo "Do you need Sirtfi to access a service?";
                     } ?>
                  Look for your home organisation below and click to email them a request.<br /><br />
               </p>
            </div>
            <div class="col-2">
               <img src="/sirtfi-logo.png" class="img-fluid rounded mx-auto d-block" alt="Sirtfi Logo"  />
            </div>
         </div>
      </div>
      <!-- Explanation -->
      <div class="container">
         <div class="row mt-1">
            <div class="col">
               <p>
                  <i>Your home organisation must comply with security
                  best practices in order for you to have access to certain research services.
                  Want more information? Visit the <a href="https://refeds.org/sirtfi">Sirtfi Homepage</a></i>.
               </p>
            </div>
         </div>
      </div>
      <!-- Hidden email -->
      <div class="container">
         <div class="row" id="email-preview" style="display: none">
            <div class="col">
               <div class="jumbotron">
                  <h1>Ready to send?</h1>
                  <div id="message-text"></div>
               </div>
            </div>
         </div>
      </div>
      <!-- Data table -->
      <div class="container">
         <div class="row mt-2">
            <div class="col-md-sm">
               <table id="idptable" class="display" style="width:100%"></table>
               <p style="color:#FFC277">
                  <?php
                     include 'table.txt.php';
                     ?>
               </p>
            </div>
         </div>
      </div>
      <script>
         var dataSet = [
         	<?php $idps = file_get_contents('table.txt'); echo $idps  ?>
         ];

         $(document).ready(function() {
         	$('#idptable').DataTable( {
         		data: dataSet,
         		columns: [
         			{ title: "Name" },
         			{ title: "Sirtfi?" },
         			{ title: "Contact" },
         		],
                         columnDefs: [
                                 { "targets": [ 1 ],
                                   "createdCell": function (td, cellData, rowData, row, col) {
                                           if ( cellData == "True" ) {
                                             $(td).css('color', 'green')
                                           } else {
                                             $(td).css('color', 'red')
                                           }
                                   }
                                 },
                                 { "targets": [ 2 ],
                                   "visible": false
                                 },
                         ],
         	} );

         	var table = $('#idptable').DataTable();
             $('#idptable tbody').on('click', 'tr', function () {
                 var data = table.row( this ).data();
                 var msg = "";

                 if ( data[1] == "False" ){
                 	msg = "Hello,<br /> I am having trouble accessing "+sp+","+
                         " because my Identity Provider, '"+data[0]+
                 	    "', is not compliant with the Sirtfi Framework."+
                         "<br /> You can find more details at https://wiki.refeds.org/display/SIRTFI/Guide+for+Federation+Participants"+
                         " <br /> Please could you help me?";
                 } else {
                 	msg = "Hello,<br /> I am having trouble accessing "+sp+", "+
                        "from my Identity Provider, '"+data[0]+"', "+
                        "even though it is compliant with the Sirtfi Framework."+
                        "<br /> Please could you help me?";
                 }
         		msg += "<br />-----------------------------" +
         		"<br />This message was drafted using sirtfi.cern.ch - for further support please contact service-desk@cern.ch"

         		document.getElementById("email-preview").style.display = "block";

         		content = '<b>To: </b>'+data[2]+'<br />\
         		<b>Subject:</b> Security Policy Requirement<br />\
         		<b>Message:</b><br />'+msg+'<br />\
         		<p><a id="send-email" class="btn btn-primary btn-lg" href="#" role="button">Send</a></p>';

         		document.getElementById("message-text").innerHTML =
         		  '<p style="font-family:consolas;">'+content+'</p>';
         		$('#send-email').click(function (e){
         	        window.location.href= 'mailto:'+data[2]
         	          +'?subject=Security Policy Requirement&body='
         	          +msg.replace(/<br \/>/g, '%0A%0A');
         	        alert("An email will be created in your mail client - thanks!");
         	    } );
            } );
         } );

      </script>
   </body>
</html>
